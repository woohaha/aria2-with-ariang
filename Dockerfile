FROM node:lts-alpine as builder

RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.ustc.edu.cn/g' /etc/apk/repositories \
 && apk update \
 && apk add git \
 && git clone https://github.com/mayswind/AriaNg.git /ariang \
 && rm /ariang/.git* -rf \
 && cd /ariang \
 && npm --registry=https://registry.npm.taobao.org install \
 && npm --registry=https://registry.npm.taobao.org install natives@1.1.6 \
 && npx gulp clean build-bundle

FROM alpine:3.10-cn

COPY --from=builder /ariang/dist /ariang
COPY ./config /conf
COPY ./rc.d/* /etc/init.d/

RUN apk update \
 && chmod +x /etc/init.d/* \
 && apk add --no-cache --update openrc aria2 darkhttpd \
 && rc-update add aria2 default \
 && rc-update add ariang default \
 && rc-update add dldir default \
 && /usr/bin/crontab /conf/crontab \
 && rc-update add crond default \
		# deal with openrc
 && sed -i 's/^\(tty\d\:\:\)/#\1/g' /etc/inittab \
 && sed -i \
        # Change subsystem type to "docker"
        -e 's/#rc_sys=".*"/rc_sys="docker"/g' \
        # Allow all variables through
        -e 's/#rc_env_allow=".*"/rc_env_allow="\*"/g' \
        # Start crashed services
        -e 's/#rc_crashed_stop=.*/rc_crashed_stop=NO/g' \
        -e 's/#rc_crashed_start=.*/rc_crashed_start=YES/g' \
        # Define extra dependencies for services
        -e 's/#rc_provide=".*"/rc_provide="loopback net"/g' \
        /etc/rc.conf \
    # Remove unnecessary services
 && rm -f /etc/init.d/hwdrivers \
           /etc/init.d/hwclock \
           /etc/init.d/hwdrivers \
           /etc/init.d/modules \
           /etc/init.d/modules-load \
           /etc/init.d/modloop \
   # Can't do cgroups
 && sed -i -E 's/^\s+cgroup_add_service/#&/g' /lib/rc/sh/openrc-run.sh \
 && sed -i 's/VSERVER/DOCKER/Ig' /lib/rc/sh/init.sh

EXPOSE 80 6800 8080

#VOLUME ["/sys/fs/cgroup","/conf"]
CMD ["/sbin/init"]

