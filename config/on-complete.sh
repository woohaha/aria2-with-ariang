#!/bin/sh

if [ $2 -eq 1 ]; then
    mv "$3" /data/_dl/
fi

echo "[$(date +'%Y-%m-%d %H:%M''%Y-%m-%d %H:%M')] $2, <a href=\"_dl/$(basename $3)\"> $3 </a> $1 <br>" >> /data/_log.html
